//
//  ChatSupportView.swift

//
//  Created by nikhil on 22/04/19.
//  Copyright © 2019 nikhil. All rights reserved.
//

import UIKit
import WebKit
class ChatSupportView: UIViewController,UIWebViewDelegate,WKNavigationDelegate {

    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var wkWebd: WKWebView!
    @IBOutlet weak var web_view: UIWebView!
    var come_from = String()
    var pament_url = String()
    @IBOutlet weak var indicatior_view: UIActivityIndicatorView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let mainColor = UserDefaults.standard.string(forKey: "mainColor"){
            self.custom_view.backgroundColor = Constants.hexStringToUIColor(hex: mainColor)
        }
        
        self.navigationItem.hidesBackButton = true
        print(pament_url)
        if #available(iOS 11,*)
        {
            let myURLString = pament_url
            let url = NSURL(string: myURLString)
            let request = NSURLRequest(url: url! as URL)
            wkWebd.isHidden = false
            web_view.isHidden = true
            wkWebd.navigationDelegate = self
            wkWebd.load(request as URLRequest)
        }
        else
        {
           
                wkWebd.isHidden = true
                web_view.isHidden = false
                let myURLString = pament_url
                let url = NSURL(string: myURLString)
                let request = NSURLRequest(url: url! as URL)
                
                web_view.delegate = self
                web_view.loadRequest(request as URLRequest)

        }
        
       
        // Do any additional setup after loading the view.
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!)
    {
        print("Strat to load")
        
         self.indicatior_view.startAnimating()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        
//        if come_from == "payment"
//        {
            let success_pay = webView.url?.absoluteString ?? ""
                //.request?.url?.absoluteString ?? ""
            print(success_pay)
            if success_pay.contains("order-received")
            {
                self.perform(#selector(success_done_view), with: nil, afterDelay: 6.0)
            }
            else if success_pay.contains("order-failed")
            {
                self.perform(#selector(done_view), with: nil, afterDelay: 6.0)
            }
            else if success_pay.contains("order-pending")
            {
                self.perform(#selector(done_view), with: nil, afterDelay: 6.0)
            }
            self.indicatior_view.stopAnimating()
            self.indicatior_view.isHidden = true
//        }
//        else
//        {
//            self.indicatior_view.stopAnimating()
//            self.indicatior_view.isHidden = true
//        }
    }
    
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
        self.indicatior_view.startAnimating()
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
//        if come_from == "payment"
//        {
            let success_pay = webView.request?.url?.absoluteString ?? ""
            print(success_pay)
            if success_pay.contains("order-received")
            {
                 self.perform(#selector(success_done_view), with: nil, afterDelay: 6.0)
            }
            else if success_pay.contains("order-failed")
            {
                self.perform(#selector(done_view), with: nil, afterDelay: 6.0)
            }
            else if success_pay.contains("order-pending")
            {
                self.perform(#selector(done_view), with: nil, afterDelay: 6.0)
        }
            
            self.indicatior_view.stopAnimating()
            self.indicatior_view.isHidden = true
//        }
//        else
//        {
//            self.indicatior_view.stopAnimating()
//            self.indicatior_view.isHidden = true
//        }
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        print(error.localizedDescription)
    }
    
    @IBAction func back_Action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func done_view()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func success_done_view()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.moveToHome()
    }

}
